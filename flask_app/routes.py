import logging
from flask import jsonify

from flask_app import app
from flask_app import threads_parser
from flask_app import videos_parser


@app.route('/')
@app.route('/index')
def index():
    return "Hello, World!"


@app.route('/threads_parser')
def thread_parser():
    threads_parser.parse()
    return jsonify({'success': True})


@app.route('/videos_parser')
def video_parser():
    try:
        videos_parser.parse()
    except Exception as e:
        logging.error(e)
    return jsonify({'success': True})
