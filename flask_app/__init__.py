import logging
from flask import Flask
from flask_app.config import Config
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from apscheduler.schedulers.background import BackgroundScheduler

app = Flask(__name__)
app.config.from_object(Config)
app.debug = True
db = SQLAlchemy(app)
admin = Admin(app, template_mode='bootstrap3')
migrate = Migrate(app, db)

from flask_app import routes, models
from flask_app import threads_parser, videos_parser

admin.add_view(ModelView(models.Thread, db.session))
admin.add_view(ModelView(models.Video, db.session))


def print_time():
    logging.error('hello world')


scheduler = BackgroundScheduler()
scheduler.add_job(threads_parser.parse, trigger='interval', minutes=5)
scheduler.add_job(threads_parser.check_inactive_threads, trigger='interval', minutes=2)
scheduler.add_job(videos_parser.parse, trigger='interval', minutes=15)
scheduler.start()
