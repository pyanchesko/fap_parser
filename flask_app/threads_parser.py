# !/usr/bin/env python3
"""
Обходит треды по 2ch API и заносит в базу треды, в названии которых есть слово fap|фап
Чтоб в дальнейщем, в админке помечать те, которые нужно парсить
"""

import logging
import requests
import re
from typing import List
from flask_app import db
from flask_app.models import Thread

BASE_THREAD_URLD = 'https://2ch.hk/{board}/res/{num}.json'
CATALOG_THREADS = 'https://2ch.hk/{board}/catalog.json'
BOARD = 'b'


def filter_threads_by_title(titles: List[Thread]) -> List[Thread]:
    pattern = re.compile(r'(fap|фап)', flags=re.IGNORECASE)
    existing_threads = [i[0] for i in db.session.query(Thread.num)]

    return list(filter(
        lambda t:
        re.search(pattern, t.title) and
        int(t.num) not in existing_threads,
        titles
    ))


def is_inactive_thread(thread: Thread):
    req = requests.get(BASE_THREAD_URLD.format(board=thread.board, num=thread.num))
    return req.status_code == 404


def check_inactive_threads():
    threads = Thread.query.all()
    for thread in threads:
        if is_inactive_thread(thread):
            thread.deleted = True
            db.session.commit()
            logging.debug('{}/{} thread deleted'.format(thread.board, thread.num))


def parse():
    req = requests.get(CATALOG_THREADS.format(board=BOARD)).json().get('threads')
    threads = [Thread(title=i.get('subject'), num=i.get('num')) for i in req]

    potential_fap_threads = filter_threads_by_title(threads)

    for thread in potential_fap_threads:
        db.session.add(thread)

    db.session.commit()
