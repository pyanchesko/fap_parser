from flask_app import db
from datetime import datetime


class ThreadModerateStatuses:
    to_parse = 1
    awaiting_moderation = 0
    not_parse = -1


class Thread(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    num = db.Column(db.Integer)
    board = db.Column(db.String(5), default='b')
    title = db.Column(db.String(200))
    moderate_status = db.Column(db.Integer,
                                default=ThreadModerateStatuses.awaiting_moderation)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    deleted = db.Column(db.Boolean(), default=False)

    def __repr__(self):
        return '<Thread {}/{}>'.format(self.board, self.num)


class Video(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    src = db.Column(db.String(64), unique=True)
    url = db.Column(db.String(128), unique=True)
    hash = db.Column(db.String(120), unique=True)
    average_color = db.Column(db.String(10))
    thread_id = db.Column(db.Integer, db.ForeignKey('thread.id'))
    created_at = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<Video {}>'.format(self.src)
