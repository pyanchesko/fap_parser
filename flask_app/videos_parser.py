# !/usr/bin/env python3
"""Обходит треды по 2ch API и скачивает .webm файлы"""
import configparser
import logging
import os
from dataclasses import dataclass
from hashlib import md5
from io import BytesIO
from typing import Iterable
from urllib.parse import urljoin

import click
import requests
from sqlalchemy import exists

from flask_app import db
from flask_app.models import Thread, Video as VideoDb


BASE_DIR = os.path.abspath(os.path.dirname(__file__))
BASE_URL = 'https://2ch.hk/'
THREAD_URL = 'https://2ch.hk/{board}/res/{num}.json'


@dataclass
class AttachmentLink:
    name: str
    relative_path: str


@dataclass
class Video:
    name: str
    body: bytes
    url: str


class FileStorage:

    def __init__(self, path=None):
        if path is None:
            self.__path = os.path.join(BASE_DIR, 'videos')
        else:
            if os.path.isabs(path):
                self.__path = path
            else:
                self.__path = os.path.join(BASE_DIR, path)

    # noinspection PyShadowingNames
    def add_to_storage(self, video: Video) -> None:
        # IDEA: возможно стоит сохранять в отдельные папки тредов
        path = os.path.join(self.__path, video.name)
        with open(path, mode='wb') as f:
            # IDEA: писать чанками
            f.write(video.body)


class VideoLinksFinder:
    """Ищет новые видео в треде"""
    def __init__(self, thread_link: str):
        self.thread_link = thread_link

    def is_video(self, item: dict) -> bool:
        return item['path'][-4:] == 'webm'

    @classmethod
    def _check_nonexistence_url_in_db(cls, url: str) -> bool:
        return not db.session.query(db.exists().where(VideoDb.url == url)).scalar()

    def get_video_links(self) -> Iterable[AttachmentLink]:
        """Возвращает генератор ссылок на видеофайлы из треда."""
        posts = requests.get(self.thread_link).json()['threads'][0]['posts']
        for post in posts:
            for attachment in post['files']:
                if self.is_video(attachment) and self._check_nonexistence_url_in_db(attachment['path']):
                    yield AttachmentLink(attachment['name'], attachment['path'])


class VideoDownloader:
    """Скачивает видео"""
    def get_video(self, attachment: AttachmentLink) -> Video:
        video_url = urljoin(BASE_URL, attachment.relative_path)
        req = requests.get(video_url, stream=True)
        if not req.ok:
            raise RuntimeError(f'Get video error {video_url}')

        # TODO: переписать эту запись в буффер с немедленным чтением.
        #  Скорее всего можно сразу формировать bytes.
        raw = BytesIO()
        raw.writelines(req)
        raw.seek(0)
        return Video(attachment.name, raw.read(), video_url)


def save_to_db(video: Video, hash: any):
    db.session.add(VideoDb(src='/'.join(['videos', video.name]),
                           hash=hash.hexdigest(),
                           url=video.url))

    db.session.commit()


# noinspection PyShadowingNames
def check_unique(video: Video) -> bool:
    md5hash = md5(video.body)
    if db.session.query(exists().where(VideoDb.hash == md5hash.hexdigest())).scalar():
        return False
    save_to_db(video, md5hash)
    return True


def load_config(path: str) -> configparser.ConfigParser:
    """Load config from the specified file and return the parsed config"""
    # Get a path to the file. If it was specified, it should be fine.
    # If it was not specified, assume it's config.ini in the script's dir.
    filename = os.path.join(BASE_DIR, 'config.ini')
    if path:
        if os.path.abspath(path):
            filename = path
        else:
            filename = os.path.join(BASE_DIR, path)

    defaults = {
        'OutputDirectory': './videos',
        'LogLevel': 'INFO',
    }

    # load from file
    config = configparser.ConfigParser(defaults)
    config.read(filename)

    level = getattr(logging, config['parser']['LogLevel'])
    logging.basicConfig(level=level)

    # Convert relative paths and paths with ~
    config['parser']['OutputDirectory'] = os.path.abspath(os.path.expanduser(config['parser']['OutputDirectory']))
    os.makedirs(config['parser']['OutputDirectory'], exist_ok=True)

    return config


# @click.command()
# @click.option('--board', default='b', type=click.STRING, help='Борда')
# @click.option('--thread', default=None, type=click.INT, help='Номер треда для скачивания видео')
# @click.option('--config', default='config.ini', type=click.STRING, help='Путь к файлу с конфигурацией')
def parse(config=None):
    thread_urls = [THREAD_URL.format(board=i[0], num=i[1])
                   for i in db.session.query(Thread.board, Thread.num).filter(Thread.moderate_status == 1,
                                                                              Thread.deleted == False)]

    logging.debug('threads: %s' % thread_urls)
    print(thread_urls)
    config = load_config(config)
    logger = logging.getLogger('default')

    for thread_url in thread_urls:
        video_links_finder = VideoLinksFinder(thread_url)
        video_links = video_links_finder.get_video_links()
        for link in video_links:
            logger.debug('Processing %s', link.name)
            # noinspection PyBroadException,PyUnusedLocal
            try:
                video: Video = VideoDownloader().get_video(link)
                if check_unique(video) is True:
                    logger.debug('unique')
                    FileStorage(path=config['parser']['OutputDirectory']).add_to_storage(video)
                    logger.debug('saved')
                else:
                    logger.debug('not unique')
            except Exception:
                logger.exception(link)
