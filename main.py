# !/usr/bin/env python3
"""Обходит треды по 2ch API и скачивает .webm файлы"""
import configparser
import logging
import os
import sqlite3
from dataclasses import dataclass
from hashlib import md5
from io import BytesIO
from typing import List, Optional, Iterable
from urllib.parse import urljoin

import click
import requests


BASE_DIR = os.path.abspath(os.path.dirname(__file__))
BASE_URL = 'https://2ch.hk/'
CATALOG_URL = 'https://2ch.hk/{board}/threads.json'
THREAD_URL = 'https://2ch.hk/{board}/res/{num}.json'


@dataclass
class AttachmentLink:
    name: str
    relative_path: str


@dataclass
class Video:
    name: str
    body: bytes
    url: str


class FileStorage:

    def __init__(self, path=None):
        if path is None:
            self.__path = os.path.join(BASE_DIR, 'videos')
        else:
            if os.path.isabs(path):
                self.__path = path
            else:
                self.__path = os.path.join(BASE_DIR, path)

    # noinspection PyShadowingNames
    def add_to_storage(self, video: Video) -> None:
        # IDEA: возможно стоит сохранять в отдельные папки тредов
        path = os.path.join(self.__path, video.name)
        with open(path, mode='wb') as f:
            # IDEA: писать чанками
            f.write(video.body)


class ThreadsFinder:
    def __init__(self, board='b'):
        self.board = board

    def get_thread_urls(self) -> List[str]:
        """Список ссылок на треды определенной доски"""
        catalog = requests.get(CATALOG_URL.format(board=self.board)).json()
        return [THREAD_URL.format(board=self.board, num=i['num']) for i in catalog['threads']]

    def find_fap_threads(self) -> List[str]:
        # TODO: Надо сделать поиск конкретно фап-тредов
        return self.get_thread_urls()[:3]


class VideoLinksFinder:
    """Ищет новые видео в треде"""
    def __init__(self, thread_link: str):
        self.thread_link = thread_link

    def is_video(self, item: dict) -> bool:
        return item['path'][-4:] == 'webm'

    @classmethod
    def _check_nonexistence_url_in_db(cls, url: str) -> bool:
        # TODO: Сделать проверку на наличие url в базе(был ли уже скачат видос)
        return True

    def get_video_links(self) -> Iterable[AttachmentLink]:
        """Возвращает генератор ссылок на видеофайлы из треда."""
        posts = requests.get(self.thread_link).json()['threads'][0]['posts']
        for post in posts:
            for attachment in post['files']:
                if self.is_video(attachment) and self._check_nonexistence_url_in_db(attachment['path']):
                    yield AttachmentLink(attachment['name'], attachment['path'])


class VideoDownloader:
    """Скачивает видео"""
    def get_video(self, attachment: AttachmentLink) -> Video:
        video_url = urljoin(BASE_URL, attachment.relative_path)
        req = requests.get(video_url, stream=True)
        if not req.ok:
            raise RuntimeError(f'Get video error {video_url}')

        # TODO: переписать эту запись в буффер с немедленным чтением.
        #  Скорее всего можно сразу формировать bytes.
        raw = BytesIO()
        raw.writelines(req)
        raw.seek(0)
        return Video(attachment.name, raw.read(), video_url)


def check_hash_in_db(cursor: sqlite3.Cursor, hash: any) -> bool:
    sql_request = "SELECT * FROM videos WHERE hash = '{}';".format(hash.hexdigest())
    result = cursor.execute(sql_request).fetchall()
    if result == []:
        return False
    return True


def save_to_db(cursor: sqlite3.Cursor, video: Video, hash: any):
    sql_request = "INSERT INTO videos VALUES('{src}', '{hash}')".format(
        src='/'.join(['videos', video.name]),
        hash=hash.hexdigest()
    )
    cursor.execute(sql_request)


# noinspection PyShadowingNames
def check_unique(cursor: sqlite3.Cursor, video: Video) -> bool:
    md5hash = md5(video.body)
    if check_hash_in_db(cursor, md5hash):
        return False
    save_to_db(cursor, video, md5hash)
    return True


def load_config(path: str) -> configparser.ConfigParser:
    """Load config from the specified file and return the parsed config"""
    # Get a path to the file. If it was specified, it should be fine.
    # If it was not specified, assume it's config.ini in the script's dir.
    filename = os.path.join(BASE_DIR, 'config.ini')
    if path:
        if os.path.abspath(path):
            filename = path
        else:
            filename = os.path.join(BASE_DIR, path)

    defaults = {
        'OutputDirectory': './videos',
        'LogLevel': 'INFO',
    }
    # load from file
    config = configparser.ConfigParser(defaults)
    config.read(filename)

    level = getattr(logging, config['parser']['LogLevel'])
    logging.basicConfig(level=level)

    # Convert relative paths and paths with ~
    config['parser']['OutputDirectory'] = os.path.abspath(os.path.expanduser(config['parser']['OutputDirectory']))
    os.makedirs(config['parser']['OutputDirectory'], exist_ok=True)

    return config


def check_db_existence(cursor):
    if (1,) in cursor.execute("SELECT count(*) FROM sqlite_master WHERE type='table' AND name='videos';").fetchall():
        return
    else:
        cursor.execute("""CREATE TABLE videos
                          (src text, hash text)
                       """)


@click.command()
@click.option('--board', default='b', type=click.STRING, help='Борда')
@click.option('--thread', default=None, type=click.INT, help='Номер треда для скачивания видео')
@click.option('--config', default='config.ini', type=click.STRING, help='Путь к файлу с конфигурацией')
def main(board, thread, config):
    # TODO: Думаю вынести подключения и коммиты в базу в одельнуб функцию,
    #  и при работе с ней, как-то вызывать через декаратор
    thread = 'https://2ch.hk/b/res/193707928.json'
    conn = sqlite3.connect("mydatabase.db")
    cursor = conn.cursor()
    check_db_existence(cursor)
    config = load_config(config)
    logger = logging.getLogger('default')
    video_links_finder = VideoLinksFinder(thread)
    video_links = video_links_finder.get_video_links()
    for link in video_links:
        logger.debug('Processing %s', link.name)
        # noinspection PyBroadException,PyUnusedLocal
        try:
            video: Video = VideoDownloader().get_video(link)
            if check_unique(cursor, video) is True:
                logger.debug('unique')
                FileStorage(path=config['parser']['OutputDirectory']).add_to_storage(video)
                conn.commit()
                logger.debug('saved')
            else:
                logger.debug('not unique')
        except Exception:
            logger.exception(link)

    conn.close()


if __name__ == '__main__':
    main()
